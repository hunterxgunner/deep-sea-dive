﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NpcMovement : MonoBehaviour
{
    public float speed; // sets the speed of the npc
    public float despawnEdge; // sets the despawn border of the npc
    public bool isFacingRight; // checks whether the npc is already facing right
    public bool goingRight; // checks whether the npc should be going left or right
    private SpriteRenderer spriteRenderer;
    private Rigidbody2D rigidBody;
    private PauseMenuScript pause;

    void Awake()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        pause = GameObject.FindWithTag("GameManager").GetComponent<PauseMenuScript>();
    }
    
    void Update()
    {
        if (!pause.isPaused)
        {
            MoveUnit();
        }
        Despawn();
    }

    // moves the unit in a certain direction
    void MoveUnit()
    {
        if (isFacingRight)
        {
            if (goingRight)
            {
                spriteRenderer.flipX = false;
                rigidBody.AddForce(new Vector2(speed, 0.0f));
            }
            else
            {
                spriteRenderer.flipX = true;
                rigidBody.AddForce(new Vector2(-speed, 0.0f));
            }
        }
        else
        {
            if (goingRight)
            {
                spriteRenderer.flipX = true;
                rigidBody.AddForce(new Vector2(speed, 0.0f));
            }
            else
            {
                spriteRenderer.flipX = false;
                rigidBody.AddForce(new Vector2(-speed, 0.0f));
            }
        }
    }

    // despawns the boat if it has reached a certain border
    void Despawn()
    {
        if (transform.position.x > despawnEdge)
        {
            Destroy(gameObject);
        }
        else if (transform.position.x < -despawnEdge)
        {
            Destroy(gameObject);
        }
    }
}