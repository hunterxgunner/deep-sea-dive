﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CleanFactorSpawner : MonoBehaviour
{
    [Header("Spawning Condition 1 Setting")]
    public float condition1; // sets the minimum amount of points for this spawner to activate, in percentage
    public float spawnTimer1; // sets the initial/minimum spawn timer
    public float spawnDifferential1; // sets the spawn delay

    [Header("Spawning Condition 2 Setting")]
    public float condition2; // sets the minimum amount of points for this spawner to activate, in percentage
    public float spawnTimer2; // sets the initial/minimum spawn timer
    public float spawnDifferential2; // sets the spawn delay

    [Header("Object Settings Setting")]
    public GameObject parentObject;
    public List<GameObject> npcs = new List<GameObject>();
    public bool goingRight; // forces the npc go to the right
    public bool spawnAnyDirection; // allow the npc to go to any direction

    private GameObject selectedNpc;
    private Stats stats;
    NpcMovement npcMovement;
    float timer = 0.0f;
    float varyTimer;
    float spawnTimer;
    float spawnDifferential;

    void Awake()
    {
        stats = GameObject.FindWithTag("GameManager").GetComponent<Stats>();
    }

    void Start()
    {
        if ((stats.weeklyTrashScore/stats.winningScore) * 100 > condition2)
        {
            varyTimer = spawnTimer2;
            spawnTimer = spawnTimer2;
            spawnDifferential = spawnDifferential1;
        }
        else if ((stats.weeklyTrashScore / stats.winningScore) * 100 > condition1)
        {
            varyTimer = spawnTimer1;
            spawnTimer = spawnTimer1;
            spawnDifferential = spawnDifferential1;
        }
        else
        {
            Debug.Log("Destroyed Fish Spawner");
            Destroy(gameObject);
        }
    }

    void Update()
    {
        timer += Time.deltaTime;
        if (timer > varyTimer)
        {
            SpawnNPC();
            timer = 0;
            varyTimer = spawnTimer + Random.Range(0.0f, spawnDifferential);
        }
    }

    public void SpawnNPC()
    {
        selectedNpc = npcs[Random.Range(1, npcs.Count + 1) - 1];
        npcMovement = selectedNpc.GetComponent<NpcMovement>();
        if (spawnAnyDirection)
        {
            GoToRandomDirection();
        }
        else
        {
            if (goingRight)
            {
                npcMovement.goingRight = true;
            }
            else
            {
                npcMovement.goingRight = false;
            }
        }
        GameObject npc = Instantiate(selectedNpc, transform.position, Quaternion.identity);
        if (parentObject)
        {
            npc.transform.parent = parentObject.transform;
        }
    }

    public void GoToRandomDirection()
    {
        int random;
        random = Random.Range(1, 100);
        if (random >= 50)
        {
            npcMovement.goingRight = true;
        }
        else
        {
            npcMovement.goingRight = false;
        }
    }
}