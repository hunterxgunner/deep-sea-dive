﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NpcSpawning : MonoBehaviour
{
    [Header("Spawning Setting")]
    public float spawnTimer; // sets the initial/minimum spawn timer
    public float spawnDifferential; // sets the spawn delay

    [Header("Object Settings Setting")]
    public GameObject parentObject;
    public List<GameObject> npcs = new List<GameObject>();
    public bool goingRight; // forces the npc go to the right
    public bool spawnAnyDirection; // allow the npc to go to any direction
    private GameObject selectedNpc;

    NpcMovement npcMovement;
    float timer = 0.0f;
    float varyTimer;

    void Start()
    {
        varyTimer = spawnTimer;
    }
    
    void Update()
    {
        timer += Time.deltaTime;
        if (timer > varyTimer)
        {
            SpawnNPC();
            timer = 0;
            varyTimer = spawnTimer + Random.Range(0.0f, spawnDifferential);
        }
    }

    public void SpawnNPC()
    {
        selectedNpc = npcs[Random.Range(1, npcs.Count + 1) - 1];
        npcMovement = selectedNpc.GetComponent<NpcMovement>();
        if (spawnAnyDirection)
        {
            GoToRandomDirection();
        }
        else
        {
            if (goingRight)
            {
                npcMovement.goingRight = true;
            }
            else
            {
                npcMovement.goingRight = false;
            }
        }
        GameObject npc = Instantiate(selectedNpc, transform.position, Quaternion.identity);
        if (parentObject)
        {
            npc.transform.parent = parentObject.transform;
        }
    }

    public void GoToRandomDirection()
    {
        int random;
        random = Random.Range(1, 100);
        if (random >= 50)
        {
            npcMovement.goingRight = true;
        }
        else
        {
            npcMovement.goingRight = false;
        }
    }
}