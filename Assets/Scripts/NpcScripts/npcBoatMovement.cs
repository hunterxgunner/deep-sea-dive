﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class npcBoatMovement : MonoBehaviour
{
    public float speed;
    public float despawnEdge;
    public bool goingRight;
    SpriteRenderer spriteRenderer;
    Rigidbody2D rigidBody;


    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }
    
    void Update()
    {
        MoveUnit();
        Despawn();
    }

    // moves the unit in a certain direction
    void MoveUnit()
    {
        if (goingRight)
        {
            spriteRenderer.flipX = true;
            rigidBody.AddForce(new Vector2(speed, 0.0f));
        }
        else
        {
            spriteRenderer.flipX = false;
            rigidBody.AddForce(new Vector2(-speed, 0.0f));
        }
    }

    // despawns the boat if it has reached a certain border
    void Despawn()
    {
        if (transform.position.x > despawnEdge)
        {
            Destroy(gameObject);
        }
        else if (transform.position.x < -despawnEdge)
        {
            Destroy(gameObject);
        }
    }
}