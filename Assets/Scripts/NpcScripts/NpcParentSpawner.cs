﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NpcParentSpawner : MonoBehaviour
{
    //spawns the game object under a parent // this is manually placed
    
    public float spawnTimer; // sets the initial/minimum spawn timer
    public float spawnDifferential; // sets the spawn delay
    public GameObject parentObject;
    public List<GameObject> npcs = new List<GameObject>();
    public bool goingRight;
    public bool spawnAnyDirection;
    private GameObject selectedNpc;
    NpcMovement npcMovement;
    float timer = 0.0f;
    float varyTimer;
    
    void Start()
    {
        varyTimer = spawnTimer;
    }

    void Update()
    {
        timer += Time.deltaTime;
        if (timer > varyTimer)
        {
            SpawnNPC();
            timer = 0;
            varyTimer = spawnTimer + Random.Range(0.0f, spawnDifferential);
        }
    }

    public void SpawnNPC()
    {
        selectedNpc = npcs[Random.Range(1, npcs.Count + 1) - 1];
        npcMovement = selectedNpc.GetComponent<NpcMovement>();
        if (spawnAnyDirection)
        {
            GoToRandomDirection();
        }
        else
        {
            if (goingRight)
            {
                npcMovement.goingRight = true;
            }
            else
            {
                npcMovement.goingRight = false;
            }
        }
        GameObject bird = Instantiate(selectedNpc, transform.position, Quaternion.identity);
        bird.transform.parent = parentObject.transform;

    }

    public void GoToRandomDirection()
    {
        int random;
        random = Random.Range(1, 100);
        if (random >= 50)
        {
            npcMovement.goingRight = true;
        }
        else
        {
            npcMovement.goingRight = false;
        }
    }
}
