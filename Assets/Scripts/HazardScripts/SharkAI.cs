﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SharkAI : MonoBehaviour {

    public Transform target;
    public float speed = 1f;
    public float timer = 0f;
    public float idleMove = 2f;
    public int randPos;
    public int moveLeft = -2;
    public int moveRight = 2;
    public float fixedRot = 0;
    // Use this for initialization
    void Start () {
        randPos = Random.Range(1, 3);
    }

	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, fixedRot, transform.eulerAngles.z);

        //transform.LookAt(target);
        if (Vector3.Distance(transform.position, target.position) <= 3)
        {
            transform.position = Vector2.Lerp(transform.position, target.position, speed * Time.deltaTime);
        }

        else if (Vector3.Distance(transform.position, target.position) > 4)
        {
            if (timer > idleMove)
            {
                if (randPos == 1)
                {
                    Vector3 moveTo = new Vector3(moveLeft * Time.deltaTime, 0, 0);
                    if (transform.position != moveTo)
                    {
                        transform.Translate(moveTo);
                    }
                }
                else if (randPos == 2)
                {
                    Vector3 moveTo = new Vector3(moveRight * Time.deltaTime, 0, 0);
                    if (transform.position != moveTo)
                    {
                        transform.Translate(moveTo);
                    }
                }
            }
        }

        if (timer > 3)
        {
            randPos = Random.Range(1, 3);
            timer = 0f;
        }
    }
}
