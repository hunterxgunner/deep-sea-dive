﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrokenBottleScript : MonoBehaviour
{
    public float damage;

    private BoxCollider2D boxCollider2D;
    private Rigidbody2D rigidBody;
    private PlayerStats playerStats;

    void Awake()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        boxCollider2D = GetComponent<BoxCollider2D>();
        playerStats = GameObject.FindWithTag("Player").GetComponent<PlayerStats>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision2D)
    {
        if (collision2D.gameObject.tag == "Player")
        {
            collision2D.gameObject.GetComponent<PlayerStats>().health -= damage;
            Destroy(gameObject);
        }
    }
}