﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudMover : MonoBehaviour
{
    // this is the script that makes the clouds move
    // it's very simple, but dependent on the parent
    // will modify if there is time
    public float speed;
    private GameObject parent;

    void Awake()
    {
        parent = GameObject.Find("Farthest Parallax");
        transform.SetParent(parent.transform);
    }

    void Update()
    {
        transform.Translate(Vector2.right * (speed * Time.deltaTime));
    }
}