﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterFlow : MonoBehaviour
{
    public float waterForce; // the force of the water (higher numbers, stronger force)
    private float storedForce; // input that is stored
    public bool activeFlow;
    private PauseMenuScript pause;
    private Timer timer;

    void Awake()
    {
        pause = GameObject.FindGameObjectWithTag("GameManager").GetComponent<PauseMenuScript>();
        timer = GameObject.FindGameObjectWithTag("GameManager").GetComponent<Timer>();
        storedForce = waterForce;
    }

    void Update()
    {
        if (activeFlow && pause.isPaused == false && timer.activeGame)
        {
            storedForce = waterForce;
        }
        else
        {
            storedForce = 0;
        }
    }

    // waterflow only works if the object has a rigidbody2D attached to it
    public void OnTriggerStay2D(Collider2D collision)
    {
        collision.attachedRigidbody.AddForce(Vector2.left * storedForce, ForceMode2D.Force);
    }
}