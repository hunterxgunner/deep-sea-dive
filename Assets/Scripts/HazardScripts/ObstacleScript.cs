﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObstacleScript : MonoBehaviour
{
    public float damage;

    private CapsuleCollider2D capsuleCollider2D;
    private PolygonCollider2D polygonCollider2D;
    private Rigidbody2D rigidBody;
    private PlayerStats playerStats;

    void Awake()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        capsuleCollider2D = GetComponent<CapsuleCollider2D>();
        polygonCollider2D = GetComponent<PolygonCollider2D>();
        playerStats = GameObject.FindWithTag("Player").GetComponent<PlayerStats>();
    }
    
    void Update()
    {
        EnterHarmlessMode();
        GravityPull();
    }

    private void OnCollisionEnter2D(Collision2D collision2D)
    {
        if (rigidBody.velocity.magnitude > 2)
        {
            if (collision2D.gameObject.tag == "Player")
            {
                collision2D.gameObject.GetComponent<PlayerStats>().health -= damage;
            }
        }
    }

    void EnterHarmlessMode()
    {
        if (transform.position.x < -30.0f)
        {
            if (capsuleCollider2D)
            {
                capsuleCollider2D.isTrigger = true;
            }
            else if (polygonCollider2D)
            {
                polygonCollider2D.isTrigger = true;
            }
            else
            {

            }
        }

        if (transform.position.x < -50.0f)
        {
            Destroy(gameObject);
        }
    }

    void GravityPull()
    {
        if (transform.position.y >= 0)
        {
            rigidBody.gravityScale = 7;
        }
        else
        {
            rigidBody.gravityScale = 0;
        }
    }
}