﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// this code should be used for both the sky and the water
// water should be murky(translucent) while the sky is solid
// water = brown-green(dirty) -> blue (clean)
// sky = skyblue (day) -> orange (afternoon)
// 1D6D32
// mid point: 1D7D00
// 1D7D64

public class ColorLerper : MonoBehaviour
{
    public Color startingColor; // the starting color
    public Color endingColor; // the ending color
    public bool timerType; // check this if the code is using the timer based system
    private Stats stats; // link the game manager system here

    public GameObject affectedObject;
    public float lowerNumber, higherNumber;
    private Timer timer;

    SpriteRenderer spriteRenderer;

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        stats = GameObject.FindWithTag("GameManager").GetComponent<Stats>();
        timer = GameObject.FindGameObjectWithTag("GameManager").GetComponent<Timer>();
    }
    
    void Update()
    {
        ChangeColor();
    }

    // functions that allows you to change the color
    void ChangeColor()
    {
        // if the object uses time to change the color
        if (timerType)
        {
            lowerNumber = timer.currentTime;
            higherNumber = timer.startingTime;
            spriteRenderer.color = Color.Lerp(endingColor, startingColor, (lowerNumber / higherNumber));
        }
        // if the object uses something else other than time, specifically the game manager
        else
        {
            lowerNumber = stats.weeklyTrashScore;
            higherNumber = stats.winningScore;
            spriteRenderer.color = Color.Lerp(startingColor, endingColor, (lowerNumber / higherNumber));
        }
    }
}

//https://www.youtube.com/watch?v=inFBnmF5eaE
//https://answers.unity.com/questions/597398/how-to-create-spriterenderer-colorlerp.html