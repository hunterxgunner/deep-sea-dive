﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerIcons : MonoBehaviour
{
    public GameObject player;
    public Image oxygen, health, inventory, clock;
    private PlayerInventory playerInventory;
    private PlayerStats stats;
    private Timer timer;

    void Awake()
    {
        player = GameObject.FindWithTag("Player");
        playerInventory = player.GetComponent<PlayerInventory>();
        stats = player.GetComponent<PlayerStats>();
        timer = GameObject.FindWithTag("GameManager").GetComponent<Timer>();
    }
    
    void Update()
    {
        OxygenBar();
        HealthBar();
        InventoryBar();
        TimeIcon();
    }

    void OxygenBar()
    {
        oxygen.fillAmount = stats.currectOxygen / stats.maxOxygen;
    }

    void HealthBar()
    {
        health.fillAmount = stats.health / 100;
    }

    void InventoryBar()
    {
        inventory.fillAmount = playerInventory.weight / playerInventory.maxWeight;
    }

    void TimeIcon()
    {
        clock.fillAmount = timer.currentTime / timer.startingTime;
    }
}