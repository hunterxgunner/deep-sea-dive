﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountdownUI : MonoBehaviour
{
    private Timer timer;
    public Text countdownText;
    public float secondsBeforeAppearing; // number of seconds before a warning timer occurs

    void Start()
    {
        timer = GameObject.FindGameObjectWithTag("GameManager").GetComponent<Timer>();
    }
    
    void Update()
    {
        DisplayCountdown();
    }

    void DisplayCountdown()
    {
        if (timer.currentTime < secondsBeforeAppearing)
        {
            countdownText.color = Color.red;
            countdownText.text = timer.currentTime.ToString("0.00");
        }
        else // this is when the player has plenty of time
        {
            countdownText.color = Color.black;
            countdownText.text = timer.currentTime.ToString("0");
        }
    }
}