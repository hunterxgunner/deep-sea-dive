﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class QuestPanel : MonoBehaviour
{
    public bool isActive; // checks if the panel is active
    public GameObject questPanel; // the quest panel itself
    
    void Update()
    {
        CheckAvailableGameObject();

        // this is if the player uses the hotkey instead
        if (Input.GetKeyDown("q"))
        {
            if (isActive)
            {
                questPanel.SetActive(false);
                isActive = false;
            }
            else
            {
                questPanel.SetActive(true);
                isActive = true;
            }
        }
    }

    void CheckAvailableGameObject()
    {
        if (!questPanel)
        {
            questPanel = GameObject.FindWithTag("QuestPanel");
        }
    }

    // this is binded in the button in the screen
    public void OnPress()
    {
        if (isActive)
        {
            questPanel.SetActive(false);
            isActive = false;
        }
        else
        {
            questPanel.SetActive(true);
            isActive = true;
        }
    }
}