﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthUI : MonoBehaviour
{
    public GameObject player;
    PlayerStats playerStats;
    public Text playerHealthUI;

    void Start()
    {
        playerStats = player.GetComponent<PlayerStats>();
    }
    
    void Update()
    {
        playerHealthUI.text = "Health: " + playerStats.health.ToString("0");
    }
}