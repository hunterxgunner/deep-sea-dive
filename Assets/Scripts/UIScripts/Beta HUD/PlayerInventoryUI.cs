﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInventoryUI : MonoBehaviour
{
    public GameObject player;
    PlayerSimpleInventory playerInventory;
    public Text playerInventoryUI;

    void Start()
    {
        playerInventory = player.GetComponent<PlayerSimpleInventory>();
    }
    
    void Update()
    {
        playerInventoryUI.text = "Inventory: " + playerInventory.playerItemWeight.ToString("0.##") + "/" + playerInventory.playerMaxItemWeight.ToString();
    }
}