﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerOxygenUI : MonoBehaviour
{
    public GameObject player;
    PlayerStats playerStats;
    public Text playerOxygenUI;
    
    void Start()
    {
        playerStats = player.GetComponent<PlayerStats>();
    }
    
    void Update()
    {
        playerOxygenUI.text = "Oxygen: " + playerStats.currectOxygen.ToString("0.00") + "(" + playerStats.oxygenTank.ToString("0.00") + ")";
    }
}