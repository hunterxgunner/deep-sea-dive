﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoatStatUI : MonoBehaviour
{
    public GameObject playerBoat;
    BoatStats boatStat;
    public Text boatTrashScoreUI;
    public Text boatTreasureValueUI;

    void Start()
    {
        boatStat = playerBoat.GetComponent<BoatStats>();
    }
    
    void Update()
    {
        boatTrashScoreUI.text = "Stored Trash Points: " + boatStat.currentTrashScore;
        //boatTreasureValueUI.text = "Stored Gold Value: " + boatStat.currentGoldValue;
    }
}