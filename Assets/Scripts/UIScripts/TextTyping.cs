﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextTyping : MonoBehaviour
{
    Text storyText; // the text stored from the text object
    string story; // stores the text variables
    public float delay; // the delay before the next text appears

    void Awake()
    {
        storyText = GetComponent<Text>();
        story = storyText.text; // stores the text from the text object, to be used later
        storyText.text = ""; // clears the text object

        StartCoroutine("PlayText");
    }

    IEnumerator PlayText()
    {
        foreach (char c in story)
        {
            storyText.text += c;
            yield return new WaitForSeconds(delay);
        }
    }
}

//sourced from: https://unitycoder.com/blog/2015/12/03/ui-text-typewriter-effect-script/