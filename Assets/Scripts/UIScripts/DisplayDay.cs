﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayDay : MonoBehaviour
{
    // all the code here is to be placed on the text field itself
    private Stats stats;
    private Text dayText;

    void Awake()
    {
        stats = GameObject.FindWithTag("GameManager").GetComponent<Stats>();
        dayText = GetComponent<Text>();
    }
    
    void Update()
    {
        ChangeText();
    }

    void ChangeText()
    {
        dayText.text = "Day " + stats.currentDay.ToString();
    }
}