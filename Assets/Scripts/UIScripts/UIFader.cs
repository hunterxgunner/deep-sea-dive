﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIFader : MonoBehaviour
{
    private Image image;
    public float startingFade;
    public float endingFade;
    private float timer;

    void Awake()
    {
        image = GetComponent<Image>();
    }

    void Start()
    {
        image.GetComponent<Image>().CrossFadeAlpha(0.0f, startingFade, false);
    }

    void Update()
    {

    }

    public void FadeIn()
    {
        image.GetComponent<Image>().CrossFadeAlpha(1.0f, endingFade, false);
    }
}