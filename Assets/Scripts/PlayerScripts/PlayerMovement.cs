﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody2D playerRigidbody;
    private SpriteRenderer spriteRenderer;
    private Animator anim;
    private PauseMenuScript pause;
    private Timer timer;
    private GameObject spawnPoint;
    public float speed; // speed of the player, actually uses force
    public bool clamp; // used for debugging, sets a clamp
    public bool mouseControls; // experimental feature that can be turned on or off
    private Vector3 mouseTarget; // experimental feature effect
    private bool traveling;
    public bool goToSpawn;

    void Awake()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        pause = GameObject.FindWithTag("GameManager").GetComponent<PauseMenuScript>();
        timer = GameObject.FindWithTag("GameManager").GetComponent<Timer>();
        spawnPoint = GameObject.FindWithTag("Respawn");
    }

    void Start()
    {
        if (goToSpawn)
        {
            transform.position = spawnPoint.transform.position;
        }
    }

    void FixedUpdate()
    {
        AnimatorCommands();

        if (pause.isPaused || timer.activeGame == false) { } // if is empty, which makes all keyboard inputs disabled
        else
        {
            if (mouseControls)
            {
                MouseInput();
            }
            else
            {
                KeyboardInput();
            }
        }
        if (clamp)
        {
            PlayerClamp();
        }

        GravityPull();
    }

    // keyboard inputs
    void KeyboardInput()
    {
        if (Input.GetKey("w") && transform.position.y < 0)
        {
            playerRigidbody.AddForce(new Vector2(0.0f, speed));
            anim.SetBool("Moving", true);
        }
        if (Input.GetKey("a"))
        {
            playerRigidbody.AddForce(new Vector2(-speed, 0.0f));
            spriteRenderer.flipX = true;
            anim.SetBool("Moving", true);
            //anim.SetInteger("SwimDirection", 0);
        }
        if (Input.GetKey("s"))
        {
            playerRigidbody.AddForce(new Vector2(0.0f, -speed));
            anim.SetBool("Moving", true);
        }
        if (Input.GetKey("d"))
        {
            playerRigidbody.AddForce(new Vector2(speed, 0.0f));
            spriteRenderer.flipX = false;
            anim.SetBool("Moving", true);
            //anim.SetInteger("SwimDirection", 1);
        }
    }

    // mouse input (experimental)
    void MouseInput()
    {
        if (Input.GetMouseButton(0))
        {
            mouseTarget = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mouseTarget.z = transform.position.z;
            traveling = true;
        }

        if (traveling)
        {
            Vector2 direction = (mouseTarget - transform.position).normalized;
            
            Quaternion toRotation = Quaternion.FromToRotation(transform.up, direction);
            transform.rotation = Quaternion.Slerp(transform.rotation, toRotation, 2.5f * Time.deltaTime);

            if (Vector3.Distance(transform.position, mouseTarget) > 1.0f)
            {
                playerRigidbody.AddForce(direction * speed);
            }
            else
            {
                traveling = false;
            }
        }
    }

    // gravity effect when over waters
    void GravityPull()
    {
        if (transform.position.y >= 0)
        {
            playerRigidbody.gravityScale = 5;
        }
        else
        {
            playerRigidbody.gravityScale = 0;
        }
    }

    // limits the player's movement to a location
    void PlayerClamp()
    {
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -45.0f, 45.0f), transform.position.y, 0);
    }

    void AnimatorCommands()
    {
        anim.SetBool("Moving", false);
        anim.SetFloat("Depth", transform.position.y);
    }
}

//https://answers.unity.com/questions/635782/moving-rigidbody2d-towards-mouse-click-using-addfo.html
//https://docs.unity3d.com/ScriptReference/Transform.LookAt.html
//https://answers.unity.com/questions/862380/how-to-slow-down-transformlookat.html