﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerHealthManager : MonoBehaviour
{
    private PlayerStats playerStats; // we get the health here
    public GameObject gradient;
    public GameObject startingScreen;
    public float waitingTime;
    private float waitTime;

    void Awake()
    {
        playerStats = GameObject.FindWithTag("Player").GetComponent<PlayerStats>();
    }

    void Start()
    {
        waitTime = waitingTime;
    }

    void Update()
    {
        if (playerStats.health <= 0)
        {
            Death();
        }
    }

    void Death()
    {
        GetComponent<Animator>().enabled = false;
        GetComponent<PlayerMovement>().enabled = false;
        gradient.GetComponent<Image>().CrossFadeAlpha(0.627451f, 0.25f, false);
        startingScreen.GetComponent<Image>().CrossFadeAlpha(1.0f, 0.75f, false);
        gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
        waitTime -= Time.deltaTime;
        if (waitTime <= 0.0f)
        {
            SceneManager.LoadScene("DeathScene");
        }

    }
}