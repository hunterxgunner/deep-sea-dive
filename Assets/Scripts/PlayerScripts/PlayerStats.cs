﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    // oxygen (player's breath) variables
    public float maxOxygen; // oxygen of the player, adjustable with upgrades
    public float currectOxygen;
    // oxygen tank variables
    public float maxOxygenTank; // oxygen tank of the player, adjustable with the player
    public float oxygenTank;
    // stamina variables
    public float maxStamina; // stamina of the player, adjustable by upgrades, also counts as a timer
    public float currentStamina;
    public float health; // health percent of the player; 40~ player has to skip a day; 20~ player has to skip 2 days; 0 is death

    private Stats stats;

    void Awake()
    {
        stats = GameObject.FindWithTag("GameManager").GetComponent<Stats>();
    }

    void Start()
    {
        maxOxygen = stats.maxOxygen;
        maxOxygenTank = stats.maxOxygenTank;
        health = stats.health;

        currectOxygen = maxOxygen;
        oxygenTank = maxOxygenTank;
        currentStamina = maxStamina;
    }
    
    void Update()
    {
        // if the player goes above a certain y-coordinate, refresh oxygen
        // else, keep draining it
        // if the player has no oxygen, deplete health
        // certain conditions also reduce health
        // if health is at a certain threshold, certain actions will happen
        // if health is at 0, proceed with a game over

        Clamps();
        //Breath();
        //UnderwaterBreathing();
        //Timer();
    }

    /*
    void Breath()
    {
        if (transform.position.y > -0.5f)
        {
            currectOxygen = maxOxygen;
        }
    }

    void UnderwaterBreathing()
    {
        if (oxygenTank > 0.0f)
        {
            oxygenTank -= Time.deltaTime;
        }
        else if (currectOxygen > 0.0f)
        {
            currectOxygen -= Time.deltaTime;
        }
        else
        {

        }

        if (currectOxygen <= 0.0f)
        {
            health -= Time.deltaTime * 10;
        }
    }

    void Timer()
    {
        if (currentStamina > 0.0f)
        {
            currentStamina -= Time.deltaTime;
        }
        else
        {
            // temporary: resets the time
            currentStamina = maxStamina;
        }
    }
    */

    void Clamps()
    {
        oxygenTank = Mathf.Clamp(oxygenTank, 0.0f, 1000.0f);
        currectOxygen = Mathf.Clamp(currectOxygen, 0.0f, 100.0f);
        currentStamina = Mathf.Clamp(currentStamina, 0.0f, 100.0f);
        health = Mathf.Clamp(health, 0.0f, 100.0f);
    }
}