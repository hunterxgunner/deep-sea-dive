﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatStats : MonoBehaviour
{
    public int currentTrashScore = 0;
    //public int currentGoldValue = 0;

    PlayerSimpleInventory playerInventory;
    GameObject player;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerInventory = player.GetComponent<PlayerSimpleInventory>();
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            // store player inventory and values to the boat
            currentTrashScore += playerInventory.playerTrashValue;
            //currentGoldValue += playerInventory.playerTreasureValue;
            // clear player inventory and values
            playerInventory.playerTrashValue = 0;
            //playerInventory.playerTreasureValue = 0;
            playerInventory.playerItemWeight = 0;
        }
    }
}