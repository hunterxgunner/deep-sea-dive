﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerBreathing : MonoBehaviour
{
    private PlayerStats playerStats;
    public GameObject gradient;
    public Color notDrowning;
    public Color drowning;

    void Awake()
    {
        playerStats = GetComponent<PlayerStats>();
    }

    void Start()
    {
        gradient.SetActive(true);
        gradient.GetComponent<Image>().CrossFadeAlpha(0.0f, 0.0f, false);
    }

    void Update()
    {
        Breath();
        UnderwaterBreathing();
    }

    // makes the player breath and grasp air when above water
    public void Breath()
    {
        if (transform.position.y > -0.55f)
        {
            playerStats.currectOxygen = playerStats.maxOxygen;
        }
    }

    // makes the player hold his breath when underwater
    public void UnderwaterBreathing()
    {
        // oxygen tank goes first all the time // does not natural replenish
        if (playerStats.oxygenTank > 0.0f)
        {
            playerStats.oxygenTank -= Time.deltaTime;
        }
        // when the oxygen tank is empty, drains from own lungs instead
        else if (playerStats.currectOxygen > 0.0f)
        {
            playerStats.currectOxygen -= Time.deltaTime;
        }
        else
        { }

        // if the player's lungs is empty, drain health
        if (playerStats.currectOxygen <= 0.0f)
        {
            playerStats.health -= Time.deltaTime * 10;
            gradient.GetComponent<Image>().CrossFadeAlpha(0.627451f, 0.25f, false);
            //drowningImage.color = Color.Lerp(drowning, notDrowning, 0.0f);
        }
        else
        {
            //drowningImage.color = Color.Lerp(notDrowning, drowning, 0.0f);
            gradient.GetComponent<Image>().CrossFadeAlpha(0.0f, 0.25f, false);
        }
    }
}
//https://answers.unity.com/questions/1208118/fading-ui-image.html
//https://answers.unity.com/questions/1121691/how-to-modify-images-coloralpha.html
//https://forum.unity.com/threads/changing-a-new-ui-images-alpha-value.289755/