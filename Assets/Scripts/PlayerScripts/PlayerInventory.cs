﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInventory : MonoBehaviour
{
    //public List<RegularItem> regularItems = new List<RegularItem>();
    public List<string> regularItemNames = new List<string>();
    private Stats playerStats;
    public float maxWeight;
    public float weight;
    public float trashScore;

    void Awake()
    {
        playerStats = GameObject.FindWithTag("GameManager").GetComponent<Stats>();
    }

    void Start()
    {
        maxWeight = playerStats.maxWeight;
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Trash")
        {
            PickUp(collision);
        }
    }

    void PickUp(Collision2D collision)
    {
        if ((maxWeight - weight) >= collision.gameObject.GetComponent<BaseItem>().itemWeight)
        {
            //regularItems.Add(collision.gameObject.GetComponent<RegularItem>());
            regularItemNames.Add(collision.gameObject.GetComponent<RegularItem>().itemName);
            trashScore += collision.gameObject.GetComponent<BaseItem>().trashScore;
            weight += collision.gameObject.GetComponent<BaseItem>().itemWeight;
            Destroy(collision.gameObject);
        }
        //foreach (RegularItem item in regularItems)
        //{
        //    trashScore += item.trashScore;
        //}
    }
}