﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerClearTrap : MonoBehaviour
{
    // this script is unused

    // have this script attached to the player
    public float delay;
    public float interactDistance;
    public GameObject trashTrap;
    public float buttonHold;
    public Text trashTrapText;
    public Image loadingBar;
    SluiceScript sluiceScript;
    StoredItems storedItems;
    private bool cleared;

    void Awake()
    {
        trashTrap = GameObject.Find("Trash Trap");
        //sluiceScript = trashTrap.GetComponent<SluiceScript>();
        storedItems = GameObject.FindWithTag("Collector").GetComponent<StoredItems>();
    }
    
    void Update()
    {
        if (Vector2.Distance(transform.position, trashTrap.transform.position) <= interactDistance * 2)
        {
            InteractHeld();
        }
        else
        {
            buttonHold = 0.0f;
            cleared = false;
        }

        if (buttonHold >= delay)
        {
            if (!cleared)
            {
                ClearTrap();
            }
        }
        InteractUI();
    }

    void InteractHeld()
    {
        if (Input.GetKeyDown("e"))
        {
            buttonHold = 0.0f;
        }
        if (Input.GetKey("e"))
        {
            buttonHold += Time.deltaTime;
        }
        else if (Input.GetKeyUp("e"))
        {
            buttonHold = 0.0f;
            cleared = false;
        }
    }

    void InteractUI()
    {
        // when the two are in proximity
        if (Vector2.Distance(transform.position, trashTrap.transform.position) <= interactDistance * 2.0f)
        {
            // when the button is held down
            if(buttonHold != 0.0f && (buttonHold / delay * 100.0f) < 100.0f)
            {
                trashTrapText.text = "Clearing Trash " + (buttonHold / delay * 100.0f).ToString("0") + "%";
            }
            else if (buttonHold != 0.0f && (buttonHold / delay * 100.0f) >= 100.0f)
            {
                trashTrapText.text = "Trash is Clear";
            }
            else
            {
                trashTrapText.text = "Hold E to Clear";
            }
        }
        else
        {
            trashTrapText.text = "";
        }

        loadingBar.fillAmount = buttonHold / delay;
    }

    // clears the trap of junk
    void ClearTrap()
    {
        // stored item score will always turn to half the amount
        storedItems.trashScore += sluiceScript.trashScore / 2.0f;
        sluiceScript.currentWeight = 0.0f;
        cleared = trashTrap;
    }

    void OnDrawGizmos()
    {
        Gizmos.color = new Color(0.0f, 0.0f, 1.0f, 0.5f);
        Gizmos.DrawSphere(transform.position, interactDistance);
        //Gizmos.DrawSphere(trashTrap.transform.position, interactDistance);
    }
}