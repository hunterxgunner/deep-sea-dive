﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemFloat : MonoBehaviour
{
    private Rigidbody2D rigidBody;
    private RegularItem regularItem;
    float itemWeight;

    // float scripts for items (usually light items)
    void Awake()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        regularItem = GetComponent<RegularItem>();
    }
    
    void Start()
    {
        itemWeight = regularItem.itemWeight;
    }

    void Update()
    {
        GravityPull();
        WaterBob();
    }

    // gravity script that increase the gravity when over water
    void GravityPull()
    {
        if (transform.position.y >= 0)
        {
            rigidBody.gravityScale = 7;
        }
        else
        {
            rigidBody.gravityScale = 0;
        }
    }
    
    // specialized script that positions the item in a certain depth, affected by the weight
    void WaterBob()
    {
        if (-gameObject.transform.position.y > itemWeight)
        {
            rigidBody.gravityScale = -1;
        }
        else
        {
            rigidBody.gravityScale = 1;
        }
    }
}