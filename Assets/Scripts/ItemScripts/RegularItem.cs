﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegularItem : BaseItem
{
    // (if possible: add item to a list/array of items in the player's inventory, no need to display)
    // (if this is the case: when hit, make it so that the item is dropped from player inventory)
    // when the player collects the object, the game object is removed from the world
    // supposedly the item is then tallied into the maximum weight
    // if the item exceeds the maximum weight, do not collect the item

    public GameObject particle; // handles particles; usually slot in is the sparkle effect
    //public bool isTreasure; // checks if the item is a treasure item
    //public float lowerValue, higherValue; // random value distribution
    public float lowerWeight, higherWeight; // random weight distribution

    /*private void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager");
    }*/

    // temporarily here before a rigidbody2D was placed
    /*public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            PickUp();
        }
    }*/

    // if the player collides with the object and has space in his inventory, pick up, else simply bumps into it and pushes it around
    /*public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            PickUp();
        }
    }*/

    // this code is unused
    void PickUp()
    {
        if (itemWeight <= (playerInventory.playerMaxItemWeight - playerInventory.playerItemWeight))
        {
            if (gameObject.GetComponent<BaseItem>().isQuestItem && gameObject.GetComponent<BaseItem>().itemName == gameManager.GetComponent<RandomQuest>().questObject1.GetComponent<BaseItem>().itemName)
            {
                gameManager.GetComponent<RandomQuest>().questCounter1++;
            }
            if (gameObject.GetComponent<BaseItem>().isQuestItem && gameObject.GetComponent<BaseItem>().itemName == gameManager.GetComponent<RandomQuest>().questObject2.GetComponent<BaseItem>().itemName)
            {
                gameManager.GetComponent<RandomQuest>().questCounter2++;
            }

            
            //playerInventory.playerItemWeight += itemWeight;
            //playerInventory.playerTrashValue += trashScore;
            //playerInventory.playerTreasureValue += goldValue;
            /*if (isTreasure)
            {
                Sparkle();
            }*/
            //Destroy(gameObject);
        }
    }

    void Sparkle()
    {
        GameObject Sparkle = Instantiate(particle, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity);
        Destroy(Sparkle, 2.0f);
    }
}

//https://www.youtube.com/watch?v=YQ7Umjp6R10