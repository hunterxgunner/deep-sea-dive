﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseItem : MonoBehaviour
{
    public string itemName; // the name of the item; currently useless
    public float itemWeight; // the weight of the item; if item goes over the player carry weight, player cannot collect item
    public int trashScore; // trash value of the item;
    public bool isQuestItem = false;
    //public int goldValue; // gold value of the item;
    public GameObject player; // script that handles player inventory
    public PlayerSimpleInventory playerInventory; // script that handles player inventory
    public SpriteRenderer spriteRenderer; // accesses the sprite renderer of the object
    public GameObject gameManager;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerInventory = player.GetComponent<PlayerSimpleInventory>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        gameManager = GameObject.FindGameObjectWithTag("GameManager");
    }
}