﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    private GameObject player; // player game object, set to private
    public float dampTime; // amount of dampening, adjustable in the editor
    private float dampener; // actual dampener
    public float trackingGap; // the gap between tracking distances
    private float gap;
    public float distance; // distance of camera from the player
    private Vector3 velocity = Vector3.zero;
    Vector3 destination;
    public bool clampMap; 
    Camera mainCamera;

    private void Awake()
    {
        mainCamera = GetComponent<Camera>();
        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }
    }

    void Start()
    {

    }

    void Update()
    {
        MoveCamera();

        if (clampMap)
        {
            Clamp();
        }
    }

    // clamps the map to restrict movement
    void Clamp()
    {
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -35.0f, 35.0f), Mathf.Clamp(transform.position.y, -2.5f, 1.0f), distance);
    }

    void MoveCamera()
    {
        if (player != null)
        {
            Vector3 delta = player.gameObject.transform.position - mainCamera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, distance));

            if (Input.GetKey("d"))
            {
                gap = trackingGap;
                dampener = dampTime;
            }
            else if (Input.GetKey("a"))
            {
                gap = -trackingGap;
                dampener = dampTime;
            }
            dampener = Mathf.Clamp(dampener, 0.0f, dampTime);
            //dampener -= Time.deltaTime; // smooth damp but it's not working

            Vector3 destination = transform.position + delta + new Vector3(gap, 0.0f);
            transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampener);
            //distance -= Input.GetAxis("Mouse ScrollWheel") * 10.0f; // changes the zoom.
        }
    }
}