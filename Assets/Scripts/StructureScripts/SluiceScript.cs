﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SluiceScript : MonoBehaviour
{
    public float maxWeight; // the max weight possible in the spawner
    public float currentWeight; // the 
    public float trashScore;
    public GameObject spawner;
    ItemSpawner itemSpawner;
    public GameObject waterFlow;
    WaterFlow flowScript;

    void Awake()
    {
        itemSpawner = spawner.GetComponent<ItemSpawner>();
        flowScript = waterFlow.GetComponent<WaterFlow>();
    }
    
    void Update()
    {
        ActiveSpawner();

        // temporary clear button
        /*if (Input.GetKeyDown("e"))
        {
            currentWeight = 0;
        }*/
    }

    void ActiveSpawner()
    {
        if (currentWeight > maxWeight)
        {
            itemSpawner.activeSpawner = false;
            flowScript.activeFlow = false;
        }
        else
        {
            itemSpawner.activeSpawner = true;
            flowScript.activeFlow = true;
        }
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Trash")
        {
            PickUp(collision);
        }
    }

    void PickUp(Collision2D other)
    {
        RegularItem regularItem;
        regularItem = other.gameObject.GetComponent<RegularItem>();

        if (currentWeight < maxWeight)
        {
            currentWeight += regularItem.itemWeight;
            trashScore += regularItem.trashScore;

            Destroy(other.gameObject);
        }

        /*if (regularItem.itemWeight <= (maxWeight - currentWeight))
        {
            currentWeight+= regularItem.itemWeight;
            trashScore += regularItem.trashScore;
            
            Destroy(other.gameObject);
            
        }*/
    }
}