﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterObjectMover : MonoBehaviour
{
    public float speed; // speed of the flow
    public float spawnEdge; // edges are x-coordinates
    public float exitEdge;

        

    void Start()
    {
        
    }
    
    void Update()
    {
        Flow();
        MovePositions();
    }

    void Flow()
    {
        transform.Translate(Vector2.left * (speed * Time.deltaTime));
    }

    void MovePositions()
    {
        if (transform.position.x <= exitEdge)
        {
            transform.position = new Vector3(spawnEdge, 1.0f, 0.0f);
        }
    }
}