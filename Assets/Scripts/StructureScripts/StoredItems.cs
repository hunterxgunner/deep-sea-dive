﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoredItems : MonoBehaviour
{
    public float trashScore; // stored trash score
    public List<string> regularItemNames = new List<string>(); // stored items, by name
    
    private RandomQuest questManager; // the quest manager/handler // this is where we get the specific quest items

    private GameObject player; // the player character
    private PlayerInventory playerInventory; // the inventory by the player character

    void Awake()
    {
        questManager = GameObject.FindWithTag("GameManager").GetComponent<RandomQuest>();

        player = GameObject.FindWithTag("Player");
        playerInventory = player.GetComponent<PlayerInventory>();
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            // store player inventory and values to the boat
            trashScore += playerInventory.trashScore;
            // clear player inventory and values
            playerInventory.trashScore = 0;
            playerInventory.weight = 0;
            foreach (string name in playerInventory.regularItemNames)
            {
                regularItemNames.Add(name);
            }
            CheckForSpecificQuestItem();
            playerInventory.regularItemNames.Clear(); // clears the list
        }
    }

    void CheckForSpecificQuestItem()
    {
        List<string> questItems = new List<string> {questManager.questObject1.GetComponent<BaseItem>().itemName, questManager.questObject2.GetComponent<BaseItem>().itemName };
        
        foreach(string items in playerInventory.regularItemNames)
        {
            if (items == questItems[0])
            {
                questManager.questCounter1++;
            }
            if (items == questItems[1])
            {
                questManager.questCounter2++;
            }
        }
    }

    /*
     * bool ContainsOnly() {
    List<string> approved = new List<string> { "Bob", "Amy" };
    foreach (item in myNames) {
        if (!approved.Contains(item)) return false;
    }
    return true;
    }
    */
} 