﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DayChangeScene : MonoBehaviour
{
    public float delay; // seconds before scene changes
    public float addHealth; // health added to player as he rests
    public float addTimer; // timer added to the game
    public float maxTimer; // timer limit to prevent adding more time
    private Stats stats;
    private Timer timer;

    void Awake()
    {
        stats = GameObject.FindWithTag("GameManager").GetComponent<Stats>();
        timer = GameObject.FindWithTag("GameManager").GetComponent<Timer>();
    }

    void Start()
    {
        stats.health += addHealth;
        if (timer.startingTime < maxTimer)
        {
            timer.startingTime += addTimer;
        }
        timer.currentTime = timer.startingTime;
    }
    
    void Update()
    {
        delay -= Time.deltaTime;
        if (delay <= 0.0f)
        {
            if (stats.currentWeek == 1)
            {
                SceneManager.LoadScene("Stage1");
            }
            else if (stats.currentWeek == 2)
            {
                SceneManager.LoadScene("Stage2");
            }
            else
            {
                SceneManager.LoadScene("Stage3");
            }
        }
    }
}