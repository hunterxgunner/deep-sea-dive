﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour
{
    // note of great importance: This script is to be placed in Game Manager
    // this script will be used to hold the data of all assets
    // should you remove this, commit not alive

    // player's stats
    [Header("Player Stats")]
    public float maxWeight; // maximum weight of the player holding items
    // oxygen (player's breath) variables
    public float maxOxygen; // oxygen of the player, adjustable with upgrades
    // oxygen tank variables
    public float maxOxygenTank; // oxygen tank of the player, adjustable with the player
    // stamina variables // remove this, this will be replaced with time limits
    public float maxStamina; // stamina of the player, adjustable by upgrades, also counts as a timer
    public float currentStamina;
    public float health; // health percent of the player; 40~ player has to skip a day; 20~ player has to skip 2 days; 0 is death

    //structure and environment stats
    public float startingTime; // the starting time of the games
    [Header("Score and Related")]
    public float trashScore; // the trash score collected by the player on the day
    public float questBonus; // bonus points obtained from quests
    public float weeklyTrashScore; // the trash score cumilated by the player on the week
    public float winningScore; // the score required to get the good ending
    public float totalTrashScore; // the trash score cumilated by the player
    public int trashCollected; // the amount of trash items stored by the player
    public int totalTrashCollected; // the total amount of trash items
    public float maximumTrapWeight; // legacy code
    [Header("Time Periods")]
    public int currentDay; // the current day
    public int currentWeek; // the current week
    
    void Start()
    {
        
    }
    
    void Update()
    {
        health = Mathf.Clamp(health, 0.0f, 100.0f);
    }
}