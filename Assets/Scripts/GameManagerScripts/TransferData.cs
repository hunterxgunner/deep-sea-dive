﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TransferData : MonoBehaviour
{
    private Stats stats;
    private Timer timer;
    private StoredItems storedItems;
    private bool dataReceived;

    void Awake()
    {
        stats = GetComponent<Stats>();
        timer = GetComponent<Timer>();
    }

    void Start()
    {
        dataReceived = false;
    }
    
    void Update()
    {
        GetData();

        if (timer.currentTime <= 0.0f)
        {
            if (!dataReceived)
            {
                stats.trashCollected = storedItems.regularItemNames.Count;
                stats.totalTrashCollected += storedItems.regularItemNames.Count;
                stats.trashScore = storedItems.trashScore;
                stats.weeklyTrashScore += storedItems.trashScore + stats.questBonus;
                dataReceived = true;
            }
        }
        else
        {
            dataReceived = false;
        }
    }

    void GetData()
    {
        if (!storedItems)
        {
            if (SceneManager.GetActiveScene().name == "Stage1"
                || SceneManager.GetActiveScene().name == "Stage2"
                || SceneManager.GetActiveScene().name == "Stage3") // keep an eye on this
            {

                storedItems = GameObject.FindWithTag("Collector").GetComponent<StoredItems>();
            }
        }
    }
}