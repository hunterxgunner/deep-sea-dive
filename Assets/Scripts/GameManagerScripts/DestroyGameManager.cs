﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyGameManager : MonoBehaviour
{
    private GameObject gameManager;
    
    void Awake()
    {
        gameManager = GameObject.FindWithTag("GameManager");
    }
    
    void Update()
    {
        
    }

    public void DestoyManager()
    {
        Destroy(gameManager);
    }
}