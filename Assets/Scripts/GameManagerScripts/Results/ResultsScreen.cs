﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultsScreen : MonoBehaviour
{
    // linked to the ResultsHanlder in the results screen

    private Stats stats; // get the stats of the player

    // text holders 
    // daily
    [Header("Daily Page")]
    public Text dayText;
    public Text dailyWeekText; // this one is for the daily page
    public Text itemsCollectedText;
    public Text scoredObtainedText;
    public Text bonusObtainedText;
    // weekly
    [Header("Weekly Page")]
    public Text weeklyText; // this one is for the weekly page
    public Text totalItemsText;
    public Text totalScoreText;
    public Image progressBar;
    public Text totalProgressText;

    // storage variables
    // universal
    private int currentWeek;
    // daily
    private int currentDay;
    private int itemsCollected;
    private float scoreObtained;
    private float bonusObtained;
    // weekly
    private int totalItems;
    private float totalScore;
    private float cleanlinessLevel;
    private float winningScore;

    void Awake()
    {
        // readies the statistics to get the data
        stats = GameObject.FindWithTag("GameManager").GetComponent<Stats>();
    }

    void Start()
    {
        // universal stat collection
        currentWeek = stats.currentWeek;
        // daily stats collection
        currentDay = stats.currentDay;
        itemsCollected = stats.trashCollected;
        scoreObtained = stats.trashScore;
        bonusObtained = stats.questBonus;

        // weekly stats collection
        totalItems = stats.totalTrashCollected;
        totalScore = stats.weeklyTrashScore;
        winningScore = stats.winningScore;
        cleanlinessLevel = totalScore/winningScore * 100;
    }
    
    void Update()
    {
        ChangeData();
        DisplayDaily();
        DisplayWeekly();
    }

    // changes the text in the daily page
    void DisplayDaily()
    {
        dayText.text = "Day: " + currentDay.ToString();
        dailyWeekText.text = "Week: " + currentWeek.ToString();
        itemsCollectedText.text = "Items collected today:\n" + itemsCollected.ToString();
        scoredObtainedText.text = "Score obtained today:\n" + scoreObtained.ToString("0");
        bonusObtainedText.text = "Task bonus obtained:\n" + bonusObtained.ToString("0");
    }

    // changes the text in the weekly page
    void DisplayWeekly()
    {
        weeklyText.text = "Week: " + currentWeek.ToString();
        totalItemsText.text = "Total items collected:\n" + totalItems.ToString();
        totalScoreText.text = "Total score obtained:\n" + totalScore.ToString("0");
        //progressBar.fillAmount = cleanlinessLevel;
        totalProgressText.text = totalScore.ToString("0") + "/" + winningScore.ToString("0")
            + " ( " + cleanlinessLevel.ToString("0") + "%)";
    }

    void ChangeData()
    {
        itemsCollectedText.text = "Items collected:\n" + itemsCollected.ToString();
        scoredObtainedText.text = "Score obtained today:\n" + scoreObtained.ToString("0");
        //totalScoreText.text = "River cleanliness level:\n" + totalScoreObtained.ToString("0")
        //    + "(" + cleanlinessLevel.ToString("0") + "%)";
    }
}