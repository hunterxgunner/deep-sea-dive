﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TransferWeek : MonoBehaviour
{
    public Button nextButton;
    private Stats stats;

    void Awake()
    {
        stats = GameObject.FindWithTag("GameManager").GetComponent<Stats>();
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeData()
    {
        stats.totalTrashScore += stats.weeklyTrashScore;
        stats.weeklyTrashScore = 0;
        stats.winningScore += 150;
        stats.currentDay = 1;
        stats.currentWeek++;
    }
}