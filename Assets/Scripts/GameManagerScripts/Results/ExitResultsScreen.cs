﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ExitResultsScreen : MonoBehaviour
{
    public string nextScreen; // should be the last screen name
    public string exitScreen; // screen that exits the game; final results screen
    public string quotaFailure;
    public Button nextButton;
    public Button exitButton;
    public float fadeTimer;
    private float timer;
    private Stats stats;

    void Awake()
    {
        stats = GameObject.FindWithTag("GameManager").GetComponent<Stats>();
    }

    void Start()
    {
        timer = fadeTimer;
    }

    public void NextDay()
    {
        DisableButtons();
        StartCoroutine(LoadSceneAfterDelay(nextScreen, quotaFailure, fadeTimer));
    }

    public void ExitGame()
    {
        DisableButtons();
        StartCoroutine(LoadSceneAfterDelay(exitScreen, quotaFailure, fadeTimer));
    }

    public void RestartWeek()
    {
        DisableButtons();
        nextScreen = "DayScene"; // this is dangerous; it relies that the DayScene is the only name for that scene
        stats.currentDay = 0;
        stats.weeklyTrashScore = 0;
        StartCoroutine(LoadSceneAfterDelay(nextScreen, quotaFailure, fadeTimer));
    }

    public void DisableButtons()
    {
        if (nextButton)
        {
            nextButton.gameObject.SetActive(false);
        }
        if (exitButton)
        {
            exitButton.gameObject.SetActive(false);
        }
    }

     /* the reason why there are two scene strings is because
      * the "nextDay" function has three functions:
      * move to the next day if it's not the 3rd day,
      * move to the next week if the quota is reached,
      * and move to the "restartWeek" scene if the quota is not reached. */
    public IEnumerator LoadSceneAfterDelay(string nextDay, string failureName, float seconds)
    {
        yield return new WaitForSeconds(seconds);
        if (stats.currentDay >= 3) // if the player is at the end of the week
        {
            // checks if the player reaches the quota; not reaching quota goes to the weekly retry screen
            if (stats.weeklyTrashScore < stats.winningScore)
            {
                SceneManager.LoadScene(failureName);
            }
            else
            {
                if (stats.currentWeek == 1)
                {
                    SceneManager.LoadScene("Cutscene1");
                }
                if (stats.currentWeek == 2)
                {
                    SceneManager.LoadScene("Cutscene2");
                }
                if (stats.currentWeek == 3)
                {
                    SceneManager.LoadScene("Cutscene3");
                }
            }
            // based on the week, load a new cutscene
        }
        else 
        {
            SceneManager.LoadScene(nextDay);
        }
    }
}