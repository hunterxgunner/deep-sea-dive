﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FinalResultsScreen : MonoBehaviour
{
    private Stats stats;
    public Text totalTrashCollectedText;
    public Text finalTallyText;
    public Text cleanlinessPercentageText;

    private int totalTrash;
    private float totalScore;
    private float percentage;

    void Awake()
    {
        stats = GameObject.FindWithTag("GameManager").GetComponent<Stats>();
    }

    void Start()
    {
        totalTrash = stats.totalTrashCollected;
        totalScore = stats.totalTrashScore;
        percentage = (stats.totalTrashScore / stats.winningScore) * 100;
    }
    
    void Update()
    {
        ChangeData();
    }
    
    void ChangeData()
    {
        totalTrashCollectedText.text = "Total Trash Collected:\n" + totalTrash.ToString();
        finalTallyText.text = "Final Tally:\n" + totalScore.ToString();
        cleanlinessPercentageText.text = "Cleanliness Percentage:\n" + percentage.ToString();
    }
}