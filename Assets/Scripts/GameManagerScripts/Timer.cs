﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour
{
    public float startingTime;
    public float currentTime = 0;
    public bool activeGame; // this bool has no correlation to pause // pause stops the time, while this disables certain scripts
    private Stats gameStats; // stores the stats
    private WaterFlow waterFlow;

    void Awake()
    {
        gameStats = GameObject.FindGameObjectWithTag("GameManager").GetComponent<Stats>();
        //waterFlow = GameObject.Find("WaterFlow").GetComponent<WaterFlow>();
    }

    void Start()
    {
        startingTime = gameStats.startingTime;
        currentTime = startingTime;
    }

    void Update()
    {
        Countdown();
    }

    void Countdown()
    {
        if (currentTime > 0.0f)
        {
            currentTime -= Time.deltaTime;
        }
        else
        {
            activeGame = false;
            /*if (SceneManager.GetActiveScene().name != "Results" || SceneManager.GetActiveScene().name != "FinalResults" || SceneManager.GetActiveScene().name != "DeathScene")
            {
                SceneManager.LoadScene("Results");
            }*/
            if (SceneManager.GetActiveScene().name == "Stage1"
                || SceneManager.GetActiveScene().name == "Stage2"
                || SceneManager.GetActiveScene().name == "Stage3")
            {
                SceneManager.LoadScene("Results");
            }
        }
    }
}