﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneTimeSpawner : MonoBehaviour
{
    [Header("Boolean Settings")]
    public bool trashSpawner; // set is the spawner is spawning trash
    public bool noRotate; // sets the object to rotate or not; used for things like clouds

    [Header("Spawning Settings")]
    public int amount; // sets the amount of items to be spawned

    [Header("Item Selection")]
    public List<GameObject> items = new List<GameObject>();
    // move these
    private GameObject selectedObject;
    private RegularItem regularItem;
    private SpriteRenderer spriteRenderer;

    [Header("Size Setting")]
    public float length;
    public float width;

    void Start()
    {
        for (int i = 0; i <= amount; i++)
        {
            if (trashSpawner)
            {
                SpawnTrash();
            }
            else
            {
                SpawnObject();
            }
        }
        Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SpawnObject()
    {
        selectedObject = items[Random.Range(1, items.Count + 1) - 1];
        spriteRenderer = selectedObject.GetComponent<SpriteRenderer>();
        if (spriteRenderer)
        {
            if (FlipSprite())
            {
                spriteRenderer.flipX = true;
            }
            else
            {
                spriteRenderer.flipX = false;
            }
        }
        if (noRotate)
        {
            Instantiate(selectedObject, transform.position - RandomVector3(), Quaternion.identity);
        }
        else
        {
            Instantiate(selectedObject, transform.position - RandomVector3(), Quaternion.Euler(0.0f, 0.0f, Random.Range(0.0f, 360.0f)));
        }
    }

    // spawns the trash object
    public void SpawnTrash()
    {
        selectedObject = items[Random.Range(1, items.Count + 1) - 1];
        regularItem = selectedObject.GetComponent<RegularItem>();
        regularItem.itemWeight = Random.Range(regularItem.lowerWeight, regularItem.higherWeight);
        spriteRenderer = selectedObject.GetComponent<SpriteRenderer>();
        if (FlipSprite())
        {
            spriteRenderer.flipX = true;
        }
        else
        {
            spriteRenderer.flipX = false;
        }
        Instantiate(selectedObject, transform.position - RandomVector3(), Quaternion.Euler(0.0f, 0.0f, Random.Range(0.0f, 360.0f)));
    }

    // creates a random vector 3
    public Vector3 RandomVector3()
    {
        Vector3 selectedVector;
        selectedVector = new Vector3(Random.Range(-width / 2.0f, width / 2.0f), Random.Range(-length / 2.0f, length / 2.0f), 0);

        return selectedVector;
    }

    // chance to flip the sprite
    private bool FlipSprite()
    {
        int rng;
        rng = Random.Range(1, 100);
        if (rng > 50)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    // draws a field that determines the spawn area
    private void OnDrawGizmos()
    {
        Gizmos.color = new Color(1.0f, 0.0f, 0.0f, 0.5f);
        Gizmos.DrawCube(transform.position, new Vector3(width, length));
    }
}