﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class RandomQuest : MonoBehaviour
{
    public GameObject spawner; // link main game spawner here; this will help create a list of items
    private Stats stats; // link the rewards system here
    private List<GameObject> items; // list of items that can be used for quests

    // holds the quest object variable
    [HideInInspector] public GameObject questObject1;
    [HideInInspector] public GameObject questObject2;

    // quest variables
    [Header("Quest Variables")]
    public int questCounter1;
    public int questCounterCap1;
    public int questCounter2;
    public int questCounterCap2;
    private bool questRewardGiven1;
    private bool questRewardGiven2;

    // quest modifiers
    [Header("Quest Modifiers")]
    public int questRewardPoints;
    public int questMinimum;
    public int questMaximum;

    // UI quest counters in game, please link them here
    [Header("Quest Displays")]
    public GameObject questText1;
    public GameObject questText2;
    
    void Start()
    {
        stats = GameObject.FindWithTag("GameManager").GetComponent<Stats>();
        items = spawner.GetComponent<ItemSpawner>().items;
        Quest();
    }
    
    void Update()
    {
        CheckAvailableGameObject();
        if (questText1 && questText2)
        {
            UpdateText();
        }
        DoQuests();
    }

    void CheckAvailableGameObject()
    {
        if (!spawner)
        {
            //spawner = GameObject.Find("Quest List"); // create new item called questList, that simply stores items that can be added to the random quest generator
            spawner = GameObject.Find("Trash Spawner");
            if (spawner) // this is only active once
            {
                items = spawner.GetComponent<ItemSpawner>().items;
                Quest();
            }
        }
        if (!questText1)
        {
            questText1 = GameObject.Find("questText1");
        }
        if (!questText2)
        {
            questText2 = GameObject.Find("questText2");
        }
    }

    // generates a quest
    void Quest()
    {
        questObject1 = items[Random.Range(1, items.Count + 1) - 1];
        questObject1.GetComponent<BaseItem>().isQuestItem = true;
        questCounterCap1 = Random.Range(questMinimum, questMaximum);

        questObject2 = items[Random.Range(1, items.Count + 1) - 1];
        while (questObject2.GetComponent<BaseItem>().itemName == questObject1.GetComponent<BaseItem>().itemName)
        {
            questObject2 = items[Random.Range(1, items.Count + 1) - 1];
        }
        questObject2.GetComponent<BaseItem>().isQuestItem = true;
        questCounterCap2 = Random.Range(questMinimum, questMaximum);

        Debug.Log("Quest Item name1: " + questObject1.name);
        Debug.Log("Quest Item name2: " + questObject2.name);

        // reset quest count
        questCounter1 = 0;
        questCounter2 = 0;
        questRewardGiven1 = false;
        questRewardGiven2 = false;
        stats.questBonus = 0;
    }

    // function that allows quest progression
    void DoQuests()
    {
        if (questCounter1 >= questCounterCap1)
        {
            FinishQuest(questText1.GetComponent<Text>());
            if (!questRewardGiven1)
            {
                stats.questBonus += questRewardPoints;
                questRewardGiven1 = true;
            }
        }
        if (questCounter2 >= questCounterCap2)
        {
            FinishQuest(questText2.GetComponent<Text>());
            if (!questRewardGiven2)
            {
                stats.questBonus += questRewardPoints;
                questRewardGiven2 = true;
            }
        }
    }

    // to be added: way to distribute quest rewards
    void FinishQuest(Text questName)
    {
        questName.text = "Task accomplished!";
        // add additional points to cleanliness level
    }

    void UpdateText()
    {
        questText1.GetComponent<Text>().text = questObject1.GetComponent<BaseItem>().itemName + "\n"
                            + questCounter1.ToString() + "/" + questCounterCap1.ToString();
        questText2.GetComponent<Text>().text = questObject2.GetComponent<BaseItem>().itemName + "\n"
                            + questCounter2.ToString() + "/" + questCounterCap2.ToString();
    }
}