﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountdownToStart : MonoBehaviour
{
    private Timer timer;
    public Text CountdownToStartText;
    public float countdownTimer;

    void Awake()
    {
        timer = GameObject.FindWithTag("GameManager").GetComponent<Timer>();
    }

    void Start()
    {
        countdownTimer += 0.5f; // this is to add a small gap between frames (I don't know how to round up
        timer.activeGame = false;
    }
    
    void Update()
    {
        Countdown();
        
    }

    void Countdown()
    {
        countdownTimer -= Time.deltaTime;
        if (countdownTimer > 0.5f)
        {
            // keeps the timer from ticking down
            timer.currentTime = timer.startingTime;
            CountdownToStartText.text = countdownTimer.ToString("0");
        }
        else
        {
            if (countdownTimer < 1.0f)
            {
                CountdownToStartText.color = Color.green;
                CountdownToStartText.text = "GO";
                timer.activeGame = true;
            }

            if (countdownTimer <= -0.5f)
            {
                CountdownToStartText.text = "";
            }
        }
        countdownTimer = Mathf.Clamp(countdownTimer, -1.0f, 10.0f);
    }

    void EndGame()
    {
        if (timer.currentTime <= 0.0f)
        {
            CountdownToStartText.color = Color.red;
            CountdownToStartText.text = "Time is up!";
        }
    }
}