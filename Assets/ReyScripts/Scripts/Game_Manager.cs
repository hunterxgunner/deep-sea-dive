﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class Game_Manager : MonoBehaviour {

    public int day = 0;
    public bool startTimer = false;
    public float transitionTimer;

    private float curTimer;
    public GameObject dayText;

	// Use this for initialization
	void Start ()
    {
        DontDestroyOnLoad(this);
	}
	
	// Update is called once per frame
	void Update ()
    {
        //checks the current scene name
        if(SceneManager.GetActiveScene().name == "SceneTransition")
        {
            dayText = GameObject.Find("DayText");
            dayText.GetComponent<Text>().text = "Day " + day;
        }

        //timer
        if(startTimer)
        {
            curTimer += Time.deltaTime;
            if(curTimer >= transitionTimer)
            {
                startTimer = false;
                curTimer = 0;
                SceneManager.LoadScene("Day1");
            }
        }

        //transition scene
		if(Input.GetKeyDown(KeyCode.Space))
        {
            startTimer = true;
            //adds day
            day++;
            //
            SceneManager.LoadScene("SceneTransition");
        }
	}
}
