﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuScript : MonoBehaviour {

    public string mainMenuScene;
    public GameObject pauseMenu;
    public bool isPaused;
	
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if(isPaused)
            {
                ResumeGame();
            }
            else
            {
                Time.timeScale = 0f;
                isPaused = true;
                pauseMenu.SetActive(true);
            }
        }

        if (SceneManager.GetActiveScene().name == mainMenuScene
            || SceneManager.GetActiveScene().name == "Results"
            || SceneManager.GetActiveScene().name == "DayScene")
        {
            pauseMenu.SetActive(false);
        }
	}

    public void ResumeGame()
    {
        isPaused = false;
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
    }
    public void ReturnToMainMenu()
    {
        Time.timeScale = 1f;
        Destroy(this.gameObject);
        SceneManager.LoadScene(mainMenuScene);
    }
}